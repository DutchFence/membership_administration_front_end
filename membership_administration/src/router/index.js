import { createRouter, createWebHistory } from 'vue-router'
import { useUserStore } from '@/stores/auth';
const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: () => import('../views/DashboardView.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/LoginView.vue'),
    meta: { requiresAuth: false }
  },
  {
    path: '/families',
    name: 'Families',
    component: () => import('../views/FamiliesView.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/memberships',
    name: 'Staffels',
    component: () => import('../views/MembershipsView.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/fiscal-years',
    name: 'Boekjaren',
    component: () => import('../views/FiscalYearsView.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/fees',
    name: 'Bedragen',
    component: () => import('../views/FeesView.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '/family/:id',
    name: 'Familie Overzicht',
    component: () => import('../views/FamilyDetailsView.vue'),
    meta: { requiresAuth: true }
  }, {
    path: '/:pathMatch(.*)*',
    name: 'Pagina bestaat niet',
    component: () => import('../views/PageNotFoundView.vue'),
    meta: { requiresAuth: false }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

router.beforeEach((to, from, next) => {
  const userStore = useUserStore();

  if (to.meta.requiresAuth && !userStore.authenticated) {
    next('/login');
  } else if (!to.meta.requiresAuth && userStore.authenticated) {
    next('/');
  } else {
    next();
  }
});

export default router;
