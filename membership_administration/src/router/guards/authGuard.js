import { useUserStore } from "@/stores/auth";
const userStore = useUserStore;
/*
** Makes a request to the api to be authenticated
** Uses auth tokens and userdata to validate 
** If not then logout otherwise continue
*/
export function validateAuthentication(to, from, next) {

  if (userStore.authenticated) {
    // User is authenticated, allow navigation to the route
    next();
  } else {
    // User is not authenticated, redirect to the login page
    next('/login');
  }
}