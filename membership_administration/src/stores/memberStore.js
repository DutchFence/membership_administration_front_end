import { ref, computed } from 'vue';
import { defineStore } from 'pinia';
import { getData } from '@/services/dataRetriever';

// Define the member store using Pinia
export const useMemberStore = defineStore('members', () => {
    // States
    const memberData = ref([]); // Holds retrieved member data
    const dataLoaded = ref(false); // Indicates whether member data has been loaded

    // Getters
    // Getter for filtering members by family ID
    const filteredMembersByFamilyId = computed(() => (familyId) => {
        if (!dataLoaded.value) {
            return []; // Return an empty array if data hasn't been loaded yet
        }
        const members = memberData.value.filter((member) => member.family_id === familyId);
        return members;
    });

    // Actions: Fetch member data
    async function fill() {
        const query = 'members'; // The data retrieval query for members

        // Fetch member data using the dataRetriever service
        const response = await getData(query);
        memberData.value = Array.isArray(response) ? response : [];
        dataLoaded.value = true; // Mark member data as loaded
    }

    // Return functions and data
    return { fill, memberData, filteredMembersByFamilyId };
});
