import { ref } from 'vue';
import { defineStore } from 'pinia';
import { getData } from '@/services/dataRetriever';

// Define the family store using Pinia
export const useFamilyStore = defineStore('families', () => {

    // States
    const familyData = ref(null); // Holds retrieved family data

    // Getter: Retrieve family by ID
    function getFamilyById(id) {
        // If familyData is available, find the family with the matching ID
        return familyData.value ? familyData.value.find(family => family.id === id) : null;
    }

    // Actions: Fetch family data
    async function fill() {
        const query = 'dashboard'; // The data retrieval query

        // Fetch data using the dataRetriever service
        const response = await getData(query);

        // Store the retrieved data in familyData
        familyData.value = response;
    }

    // Return data and actions
    return { familyData, fill, getFamilyById };
});
