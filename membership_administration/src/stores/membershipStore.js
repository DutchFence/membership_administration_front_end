import { ref } from 'vue';
import { defineStore } from 'pinia';
import { getData } from '@/services/dataRetriever';


export const useMembershipStore = defineStore('memberships', () => {

    // States
    const membershipData = ref(null);

    //Actions

    // Get membership data
    async function fill() {
        const query = 'memberships';
        membershipData.value = await getData(query);
    }
    return { membershipData, fill };
});
