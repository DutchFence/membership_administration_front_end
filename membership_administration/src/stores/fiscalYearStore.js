import { ref } from 'vue';
import { defineStore } from 'pinia';
import { getData } from '@/services/dataRetriever';


export const useFiscalYearStore = defineStore('fiscalYears', () => {
    // States
    const fiscalYearData = ref([]);

    // Actions

    // Get data from fiscal years
    async function fill() {
        const query = `fiscalyears`;
        fiscalYearData.value = await getData(query);
    }
    return { fiscalYearData, fill };
});
