import { ref, computed } from 'vue';
import { defineStore } from 'pinia';
import axios from 'axios';
import Cookies from 'js-cookie';
import { encryptToken, decryptToken } from '@/services/encryption';
import { useRouter } from 'vue-router';

// Helper function: Set a value in the local storage
function setLocalStorage(key, value) {
  localStorage.setItem(key, value);
}

// Helper function: Remove a value from local storage
function removeLocalStorage(key) {
  localStorage.removeItem(key);
}

// Define the user store using Pinia
export const useUserStore = defineStore('auth', () => {
  // States
  const userData = ref(localStorage.getItem('user')); // Current user data
  const authenticated = ref(localStorage.getItem('authenticated')); // Authentication status
  const router = useRouter();

  // Compute the parsed user data
  const parsedUserData = computed(() => {
    if (userData.value && typeof userData.value === 'string') {
      return JSON.parse(userData.value);
    }
    return null;
  });

  // Actions

  // Get CSRF token from the server
  async function getToken() {
    await axios.get('/sanctum/csrf-cookie');
    const csrfToken = Cookies.get('XSRF-TOKEN');
    axios.defaults.headers.common['X-XSRF-TOKEN'] = csrfToken;
    return csrfToken;
  }

  // Login action
  async function login(email, password) {
    const response = await axios.post('/api/login', { email, password });
    return response.data;
  }

  // Sign-up action
  async function signUp(email, password) {
    try {
      userData.value = null;
      removeLocalStorage('user');
      removeLocalStorage('authenticated');
      authenticated.value = false;

      // Get CSRF token and login
      await getToken();
      const response = await login(email, password);
      const data = response.data;

      // Encrypt token and manage user data
      const token = encryptToken(data.token, process.env.VUE_APP_SECRET_KEY);
      userData.value = data.user;
      authenticated.value = true;

      // Store data in local storage
      setLocalStorage('user', JSON.stringify(userData.value));
      setLocalStorage('token', token);
      setLocalStorage('authenticated', authenticated.value);

      // Set authorization header and navigate
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + decryptToken(token, process.env.VUE_APP_SECRET_KEY);
      router.push('/');
    } catch (error) {
      throw error;
    }
  }

  // Sign-out action
  async function signOut() {
    await axios.post('api/logout');
    userData.value = null;
    authenticated.value = false;
    removeLocalStorage('user');
    removeLocalStorage('authenticated');
    router.push('/login');
  }

  // Return data and actions
  return { userData, parsedUserData, signUp, signOut, authenticated };
});
