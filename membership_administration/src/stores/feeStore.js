import { ref, computed } from 'vue';
import { defineStore } from 'pinia';
import { getData } from '@/services/dataRetriever';

// Define the fee store using Pinia
export const useFeeStore = defineStore('fees', () => {
    // States
    const feeData = ref([]); // Holds retrieved fee data

    // Getters: Retrieve fee information
    // Getter for member's fee based on membership ID
    const getMemberFee = computed(() => (membershipId) => {
        const fee = feeData.value.find((fee) => fee.membership_id === membershipId);
        return fee.sum;
    });

    // Getter for family's total fee based on an array of family members
    const getFamilyFee = computed(() => (members) => {
        let sum = 0;
        members.forEach(member => {
            const fee = feeData.value.find((fee) => fee.membership_id === member.membership_id);
            sum += fee.sum;
        });
        return sum;
    });

    // Actions: Fetch fee data
    async function fill() {
        const query = `fees`; // The data retrieval query for fees

        // Fetch fee data using the dataRetriever service
        feeData.value = await getData(query);
    }

    // Return data and actions 
    return { feeData, fill, getMemberFee, getFamilyFee };
});
