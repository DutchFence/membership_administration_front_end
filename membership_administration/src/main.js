import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import { createPinia } from 'pinia';
import axios from 'axios';
import { decryptToken } from './services/encryption';
import '@/assets/css/main.css';

// Set default values for axious
axios.defaults.withCredentials = true;
axios.defaults.baseURL = process.env.VUE_APP_API_URL;

// Check for bearer token and set it
const token = localStorage.getItem('token');
if (token) {
  const decryptedToken = decryptToken(token, process.env.VUE_APP_SECRET_KEY);
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + decryptedToken;
}

createApp(App)
  .use(router)
  .use(createPinia())
  .mount('#app');
  