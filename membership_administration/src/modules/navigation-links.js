import { reactive } from 'vue';
let linkId = 1
export const links = reactive([
  {
    id: linkId++,
    label: 'Dashboard',
    url: '/',
    icon: 'podium'
  },
  {
    id: linkId++,
    label: 'Families',
    url: '/families',
    icon: 'people'
  },
  {
    id: linkId++,
    label: 'Lidmaatschappen',
    url: '/memberships',
    icon: 'pricetag'
  },
  {
    id: linkId++,
    label: 'Boekjaren',
    url: '/fiscal-years',
    icon: 'calendar'
  },
  {
    id: linkId++,
    label: 'Contributies',
    url: '/fees',
    icon: 'cash'
  },
]);

export const footerlinks = reactive([
  {
    id: linkId++,
    label: 'Disclaimer',
    url: 'https://www.merriam-webster.com/dictionary/disclaimer'
  },
  {
    id: linkId++,
    label: 'Copyright',
    url: 'https://www.copyright.gov/what-is-copyright/'
  },
  {
    id: linkId++,
    label: 'Report a bug',
    url: 'mailto:t.w.vanwijnen@gmail.com'
  }
]);
