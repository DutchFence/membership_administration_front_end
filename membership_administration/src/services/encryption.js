import CryptoJS from 'crypto-js';

function encryptToken(token, encryptionKey) {
    const encryptedToken = CryptoJS.AES.encrypt(token, encryptionKey).toString();
    return encryptedToken;
  }

function decryptToken(encryptedToken, encryptionKey) {
    const decryptedToken = CryptoJS.AES.decrypt(encryptedToken, encryptionKey).toString(CryptoJS.enc.Utf8);
    return decryptedToken;
  }
  export {encryptToken, decryptToken};
  