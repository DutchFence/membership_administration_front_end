import axios from "axios";
export async function getData(query = "") {
    try {
        const response = await axios.get(`api/${query}`);
        const familyData = await response.data.data;
        return familyData;
    } catch (e) {
        return e;
    }
}